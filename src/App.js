import './App.css';
import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import { Box } from '@mui/system';

const options = [
  {label: 'dev' },
  {label: 'marketing'},
  {label: 'commerce' },
  {label: 'produit' }, 
  {label:'finance' }, 
  {label:'autre'}
];

function ComboBox() {
  return (
    <Box sx={{ 
      gap: 5, 
      display: "flex" , 
      flexDirection: "column" , 
      alignItems:"center" , 
      justifyContent:"center",
      alignContent:"center",
      minHeight: "100vh"
      }}>
        <TextField 
          sx={{ width: 300 , color: 'blue'}} 
          id="standard-basic" 
          label="email" 
          variant="standard">
        </TextField>
        <Autocomplete
          aria-label="split button"
          disablePortal
          id="combo-box-demo"
          options={options}
          sx={{ width: 300 , color: 'blue'}}
          renderInput={(params) => <TextField {...params} label="select" />}
        />
    </Box>
  );
}

export default ComboBox